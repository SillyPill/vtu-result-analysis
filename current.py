# All imports
import requests
import http.client
from urllib.request import urlopen
from bs4 import BeautifulSoup
import os
import sys
import csv
import matplotlib.pyplot as plt
import re
import pandas as pd
import tkinter
from fpdf import FPDF
from collcodes import college_codes

# VTU result URL
url1 = "http://results.vtu.ac.in/vitaviresultcbcs/resultpage.php"

urltimeout = 15  # 15 or 20 if server is under load, 5 if not
numbererror = 4  # 5 if server is slow or else 3 or 4 is okay
numberskips = 4  # 5 if server is slow or else 3 or 4 is okay

def getResult(usn):
    try:
        # QUERY WEBSITE FOR RESULT
        response = requests.post(url1, data={'lns': usn}, timeout=urltimeout)
        soup = BeautifulSoup(response.content, "html.parser")
        tables = soup.findChildren("div", {"class": "divTableBody"})
        rows = tables[0].findChildren("div", {"class": "divTableRow"})

        # GET THE NAME OF RESULT
        lname = ""
        names = soup.find("td", {"style": "padding-left:15px"})
        for name in names:
            name2 = name.string
            lname = name2

        # GET THE MARKS AND FORMAT SGPA
        ll = 1
        i1 = i2 = i3 = i4 = i5 = i6 = i7 = i8 = 0
        sgpa = 0
        valuen = 4
        valuesum = 0
        gradesum = 0

        i1 = rows[1].findAll("div", {"class": "divTableCell"})[4].string
        i2 = rows[2].findAll("div", {"class": "divTableCell"})[4].string
        i3 = rows[3].findAll("div", {"class": "divTableCell"})[4].string
        i4 = rows[4].findAll("div", {"class": "divTableCell"})[4].string
        i5 = rows[5].findAll("div", {"class": "divTableCell"})[4].string
        i6 = rows[6].findAll("div", {"class": "divTableCell"})[4].string
        i7 = rows[7].findAll("div", {"class": "divTableCell"})[4].string
        i8 = rows[8].findAll("div", {"class": "divTableCell"})[4].string

        ii1 = rows[1].findAll("div", {"class": "divTableCell"})[2].string
        ii2 = rows[2].findAll("div", {"class": "divTableCell"})[2].string
        ii3 = rows[3].findAll("div", {"class": "divTableCell"})[2].string
        ii4 = rows[4].findAll("div", {"class": "divTableCell"})[2].string
        ii5 = rows[5].findAll("div", {"class": "divTableCell"})[2].string
        ii6 = rows[6].findAll("div", {"class": "divTableCell"})[2].string
        ii7 = rows[7].findAll("div", {"class": "divTableCell"})[2].string
        ii8 = rows[8].findAll("div", {"class": "divTableCell"})[2].string

        subcode1 = rows[1].findAll("div", {"class": "divTableCell"})[0].string
        subcode2 = rows[2].findAll("div", {"class": "divTableCell"})[0].string
        subcode3 = rows[3].findAll("div", {"class": "divTableCell"})[0].string
        subcode4 = rows[4].findAll("div", {"class": "divTableCell"})[0].string
        subcode5 = rows[5].findAll("div", {"class": "divTableCell"})[0].string
        subcode6 = rows[6].findAll("div", {"class": "divTableCell"})[0].string
        subcode7 = rows[7].findAll("div", {"class": "divTableCell"})[0].string
        subcode8 = rows[8].findAll("div", {"class": "divTableCell"})[0].string

        var1 = 0
        for row in rows:
            volume = row.findAll("div", {"class": "divTableCell"})[4].string

            if var1 != 0:
                value = int(volume)
                if (var1 == 7 or var1 == 8):
                    var2 = 2
                else:
                    var2 = 4
                if(value >= 40 and value < 45):
                    value2 = 4
                elif(value >= 45 and value < 50):
                    value2 = 5
                elif(value >= 50 and value < 60):
                    value2 = 6
                elif(value >= 60 and value < 70):
                    value2 = 7
                elif(value >= 70 and value < 80):
                    value2 = 8
                elif(value >= 80 and value < 90):
                    value2 = 9
                elif(value >= 90):
                    value2 = 10
                elif(value < 40):
                    value2 = 0
                gradesum = gradesum+(value2*var2)

            var1 = var1+1
        sgpa = round((gradesum/28), 2)

        RESULT = dict()
        RESULT['College Code'] = usn[:3]
        RESULT[subcode1] = i1
        RESULT[subcode2] = i2
        RESULT[subcode3] = i3
        RESULT[subcode4] = i4
        RESULT[subcode5] = i5
        RESULT[subcode6] = i6
        RESULT[subcode7] = i7
        RESULT[subcode8] = i8
        RESULT[subcode1 + "_internal"] = ii1
        RESULT[subcode2 + "_internal"] = ii2
        RESULT[subcode3 + "_internal"] = ii3
        RESULT[subcode4 + "_internal"] = ii4
        RESULT[subcode5 + "_internal"] = ii5
        RESULT[subcode6 + "_internal"] = ii6
        RESULT[subcode7 + "_internal"] = ii7
        RESULT[subcode8 + "_internal"] = ii8
        RESULT['SGPA'] = sgpa
        RESULT['Name'] = lname
        RESULT['USN'] = usn

        # print(RESULT)
        # print(usn, lname, i1, i2, i3, i4, i5, i6, i7, i8, sgpa)
        return (RESULT)

    except Exception as e:
        print('Invalid USN found')
        return 0

# college code lists


# clist = ["1CD", "1CG", "1CE", "1DT", "1DS", "1DB", "1DA", "1CC", "1GV", "1EC",
#          "1EP", "1EW", "1GS", "1GC", "1GA", "1GD", "1SK", "1GG", "1HK", "1HM",
#          "1IC", "1II", "1JV", "1JS", "1JT", "1KS", "1KI"]

clist = ["1DS", "1CC","1JS", "1JT", "1KS", "1KI"]

# GET USN LIST FROM A FILE

usnlist = []
result_list = []
all_columns = []

# f = open('usnlist.txt')
# print('Generating the USN list')
# for line in f:
#     usnlist.append(line.strip())

# for usn in usnlist:
#     # getResult(usn)
#     student_result = getResult(usn)
#     if student_result == 0:
#         continue
#     else:
#         result_list.append(student_result)
#         all_columns = list( set( student_result.keys() ) | set(all_columns) )
#         print(student_result)

batch_code = str(input("\nEnter the Batch Code : "))
branch_code = str(input('\nEnter the Branch codes in the USN : '))
bat = batch_code + branch_code


# GET USN LIST FROM LOGIC

for ccode in college_codes:
    roll_no = 0
    tr_usn = ccode + bat

    for roll_no in range(1,200):

        if roll_no < 10:
            tf_usn = tr_usn + "00" + str(roll_no)
        elif roll_no < 100:
            tf_usn = tr_usn + "0" + str(roll_no)
        else:
            tf_usn = tr_usn + str(roll_no)

        print(tf_usn)
        student_result = getResult(tf_usn)
        if student_result == 0:
            continue
        else:
            result_list.append(student_result)
            all_columns = list(set(student_result.keys()) | set(all_columns))


print('Generating the CSV file...')
all_columns = sorted(all_columns, reverse=True)
print(all_columns)

with open(bat+'.csv', 'w', newline='') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=all_columns, dialect='excel')
    writer.writeheader()
    for resulti in result_list:
        writer.writerow(resulti)

print(' DONE')

del result_list

all_subjects = all_columns[4:]

regex = re.compile('\_internal')
filtered = filter(lambda i: not regex.search(i), all_subjects) 
all_subjects = [i for i in all_subjects if not regex.search(i)]


print(all_subjects)


def histogram(data, n_bins, cumulative=False, x_label="", y_label="", title=""):
    _, ax = plt.subplots()
    ax.hist(data, n_bins=n_bins, cumulative=cumulative, color='#539caf')
    ax.set_ylabel(y_label)
    ax.set_xlabel(x_label)
    ax.set_title(title)


for sub_code in all_subjects:

    df = pd.read_csv(bat + '.csv')

    # sub_code = '15CS53'
    sub_internal = sub_code + "_internal"
    college_frame = df[['USN', sub_code, sub_internal]
                       ][df['College Code'] == '1KS']
    print( str(college_frame) )
    if college_frame[sub_code].count() == 0:
        print('No student of ' + sub_code + " ...Skipping")
        continue

    print('For subject : ', sub_code, '\n\n')

    uni_avg = round(float(df[sub_code].mean()),2)
    coll_avg = round(float(college_frame[sub_code].mean()),2)

    uni_iavg = round(float(df[sub_internal].mean()),2)
    coll_iavg = round(float(college_frame[sub_internal].mean()),2)

    uni_perc = round(float(1-df[[sub_code,]] [df[sub_code] < 40].count()/df[sub_code].count())*100,2)

    no_of_coll_students = int(college_frame[[sub_code]].count())
    no_pass_coll = int(college_frame[[sub_code]][college_frame[sub_code]>=40].count())
    coll_perc = round(float(no_pass_coll/no_of_coll_students)*100,2)

    uni_max = int(df[[sub_code]].max())
    coll_max = int(college_frame[[sub_code]].max())

    print('University Average Score is : ', uni_avg)
    print('College Average Score is : ', coll_avg)

    print('\nUniversity Average Internals Score is : ', uni_iavg)
    print('College Average Internals Score is : ', coll_iavg)

    print('\nUniversity result % = ', uni_perc)

    print( '\nNo. Of Students attempted the exam : ', no_of_coll_students)
    print( 'No. Of Students from passed : ', no_pass_coll)
    print( 'Result % = ', coll_perc) 


    print('\nMax score in the University : ', uni_max)
    print('Max score in the College : ', coll_max)

    fig, ax = plt.subplots()
    graph = df[sub_code].plot.hist(bins=50, title='University marks distribution Histogram')
    # print(fig)
    # print(ax)
    # plt.boxplot(x=list(df[sub_code]))
    # print(list(college_frame[sub_code]))
    # print(list(df[sub_code]))
    boxframe = pd.concat([college_frame[sub_code],df[sub_code]], axis=1)
    boxframe.columns=[ 'College', 'University' ]
    boxgraph = boxframe.plot.box(label='Marks', title='Box Graph').get_figure()
    print(type(boxgraph))
    print(type(fig))
    fig.savefig('hists.png')
    boxgraph.savefig('boxgr.png')
    # plt.show()
    # print(df.head())
    # print(type(df.describe()))

    pdf = FPDF()
    pdf.add_page()
    pdf.set_font('times', 'B', 24)
    pdf.write(8, 'Results Analysis for subject '+ sub_code)
    pdf.set_font('times', '', 12)
    pdf.write(5, '\n\n\n')
    pdf.write(5, '\nUniversity Average Score is : ' + str(uni_avg))
    pdf.write(5, '\nCollege Average Score is : ' + str(coll_avg))
    pdf.write(5, '\n')
    pdf.write(5, '\nUniversity Average Internals Score is : ' + str(uni_iavg))
    pdf.write(5, '\nCollege Average Internals Score is : ' + str(coll_iavg))
    pdf.write(5, '\n')
    pdf.write(5, '\nUniversity Pass % = ' + str(uni_perc) + '%')
    pdf.write(5, '\n')
    pdf.write(5, '\nNo. Of Students attempted the exam : ' + str(no_of_coll_students))
    pdf.write(5, '\nNo. Of Students passed : ' + str(no_pass_coll))
    pdf.write(5, '\n')
    pdf.write(5, '\nResult : ' + str(coll_perc) + '%')
    pdf.write(5, '\n')
    pdf.write(5, '\nMax score in the University : ' + str(uni_max))
    pdf.write(5, '\nMax score in the College : ' + str(coll_max))
    pdf.write(5, '\n')
    pdf.add_page
    pdf.image('boxgr.png', w=200)
    pdf.image('hists.png', w=200)
    pdf.write(5, '\n' + str(college_frame[['USN', sub_code, ]]))
    pdf.output(sub_code+'.pdf', 'F')

