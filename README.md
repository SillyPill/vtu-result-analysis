# Result analysis script for VTU

This script uses Beautiful Soup to scrape required results from the VTU
website.

Then, uses pandas to perform some simple analysis and comparision between
college performance and university performance for each subject.

Output is presented as PDF for each subject. The script also dumps all the
scraped data into CSV files.

# VTU changes their reults portal frequently

Since the script uses web scraping technology, some modifications might be
necessary before the results can be accessed sucessfully.

# How to run

```python current.py```

the ```current.py``` calls the other python files. Hence. they are necessary.
Do not delete them.
